#include <iostream>

#pragma once
class Block
{
public:
	Block(int board[16][10]);
	~Block();
	void printP();
	void move(char move);
};