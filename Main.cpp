#include <iostream>
#include <thread>
#include <chrono>
#include <conio.h>
#include "Block.h"

int const BOARDWIDTH = 10;
int const BOARDHEIGTH = 16;
int board[BOARDHEIGTH][BOARDWIDTH];

void drawboard(int board[BOARDHEIGTH][BOARDWIDTH]) {
	using std::cout;
	using std::endl;

	system("CLS");
	for (int i = 0; i < BOARDHEIGTH; i++) {
		cout << "#";
		for (int j = 0; j < BOARDWIDTH; j++) {
			if (board[i][j] == 0) cout << " ";
			else cout << board[i][j];
		}
		cout << "#" << endl;
	}
	cout << "############" << endl;
}

void move(Block block) {
	char move;
	while (true)
	{
		move = _getch();
		std::cout << move;
		block.move(move);
		drawboard(board);
	}
}


void moveBlocks(int board[BOARDHEIGTH][BOARDWIDTH], Block block) {
	while (true) {
		block.printP();
		drawboard(board);
		std::this_thread::sleep_for(std::chrono::milliseconds(600));
	}
}

int main() {


	for (int i = 0; i < BOARDHEIGTH; i++) {
		for (int j = 0; j < BOARDWIDTH; j++) {
			board[i][j] = 0;
		}
	}

	Block block = Block::Block(board);
	
	std::thread t1(moveBlocks, board, block);
	std::thread t2(move, block);

	t1.join();
	t2.join();
	

	return 0;
}
