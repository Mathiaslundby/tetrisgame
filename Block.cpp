#include "Block.h"

int *block[3][2];

Block::Block(int board[16][10])
{
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 2; j++) {
			block[i][j] = &board[i][j];
		}
	}

	*block[0][0] = 2;
	*block[0][1] = 2;
	*block[1][1] = 2;
	*block[2][1] = 2;
}

Block::~Block()
{
}

void Block::printP() {

	for (int i = 2; i >= 0; i--) {
		for (int j = 0; j < 2; j++) {
			int *val = block[i][j];
			block[i][j] += 10;
			*block[i][j] = *val;
			*val = 0;
		}
	}
}

void Block::move(char move) {
	int movement = 0;

	switch (move)
	{
	case 'd': movement = 1;
		break;
	case 'a': movement = -1;
		break;
	case 's': movement = 10;
		break;
	default:
		break;
	}


	for (int i = 2; i >= 0; i--) {
		for (int j = 0; j < 2; j++) {
			int *val = block[i][j];
			block[i][j] += movement;
			*block[i][j] = *val;
			*val = 0;
		}
	}
}
